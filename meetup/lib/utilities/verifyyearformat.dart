class VerifyYearFormat {
  //only four digit year supported
  static final String YEARFORMATEXCEPTION = 'Please enter 4 digit year';
  String status(String myYear) {
    // provide form validation status to the form
    // return null or prompt string
    String pattern = '\\d{4}';
    final RegExp regExp = new RegExp(pattern);
    if (!regExp.hasMatch(myYear) || myYear.length > 4)
      return YEARFORMATEXCEPTION;
    else
      return null;
  }
}
