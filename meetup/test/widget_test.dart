// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:meetup/main.dart';

void main() {
  const String TITLE = 'Meetup';
  const String FIELDTITLE = 'Year:';
  const String BUTTONTEXT = 'Submit';
  const String HINTTEXT = 'Which year?';
  const String EMPTYFIELD = 'Please enter a 4 digit year';
  final titleFieldFinder = find.text(FIELDTITLE);
  final titleFinder = find.text(TITLE);
  final hintTextFinder = find.text(HINTTEXT);
  final buttonTextFinder = find.text(BUTTONTEXT);
  final Type dropdownButtonType = DropdownButton<String>(
    onChanged: (_) {},
    items: const <DropdownMenuItem<String>>[],
  ).runtimeType;
  final dropDownButtonFinder = find.byType(dropdownButtonType);
  group('Home State', () {
    testWidgets('Default UI Test', (WidgetTester tester) async {
      // Build our app and trigger a frame.
      await tester.pumpWidget(MyApp());
      expect(titleFinder, findsOneWidget);
      expect(hintTextFinder, findsOneWidget);
      expect(buttonTextFinder, findsOneWidget);
      expect(find.byType(TextFormField), findsOneWidget);
      expect(dropDownButtonFinder, findsNWidgets(3));
      expect(titleFieldFinder, findsOneWidget);
    });
  });
  group('Messages', () {
    testWidgets('Check Validation String', (WidgetTester tester) async {
      final emptyFieldMessageFinder = find.text(EMPTYFIELD);
      await tester.pumpWidget(MyApp());
      // Tap the 'Submit' Button and trigger validation.
      await tester.tap(buttonTextFinder);
      await tester.pump();
      expect(emptyFieldMessageFinder, findsOneWidget);
    });
  });
}
