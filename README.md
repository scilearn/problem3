# Meetup code challenge [![pipeline status](https://gitlab.com/scilearn/problem3/badges/master/pipeline.svg)](https://gitlab.com/scilearn/problem3/-/commits/master)
[problems](https://bitbucket.org/snippets/scilearn/xez77B)
## Getting Started
[Install Flutter](https://flutter.dev/docs/get-started/editor)


*  Open this project in Android Studio
*  Connect a device or emulator
*  The source code is in /lib/

You can run the application from the IDE or from the terminal:
*  Type `flutter run` in the source folder to run the application

*  Tests are in the /test folder see: [Testing Flutter app] (https://flutter.dev/docs/testing)
You can run the tests from the IDE or from the terminal:
*  Type `flutter test` in the source folder to run tests with flutter test
*  If you have build problems you may need to [Upgrade Flutter](https://flutter.dev/docs/development/tools/sdk/upgrading)

Alternatively you can run builds with a [Docker Image for Flutter](https://github.com/cirruslabs/docker-images-flutter) and install the apk or app manually.

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.