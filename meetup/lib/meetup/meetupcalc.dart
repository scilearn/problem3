import 'package:intl/intl.dart';
import 'package:meetup/utilities/verifyyearformat.dart';

class MeetupCalc {
  final Map dayInstanceString = new Map();
  final Map daysOfTheWeek = new Map();
  static final String INSTANCEARGUMENTERROR = 'Invalid option'; //Error messages
  static final String DAYOFWEEKARGUMENTERROR = 'Not a valid day';
  MeetupCalc() {
    // seed maps
    dayInstanceString['last'] = 5;
    dayInstanceString['1st'] = 1;
    dayInstanceString['first'] = 1;
    dayInstanceString['2nd'] = 2;
    dayInstanceString['second'] = 2;
    dayInstanceString['3rd'] = 3;
    dayInstanceString['third'] = 3;
    dayInstanceString['4th'] = 4;
    dayInstanceString['forth'] = 4;
    dayInstanceString['teenth'] = 5;
    daysOfTheWeek['Monday'] = 1;
    daysOfTheWeek['Tuesday'] = 2;
    daysOfTheWeek['Wednesday'] = 3;
    daysOfTheWeek['Thursday'] = 4;
    daysOfTheWeek['Friday'] = 5;
    daysOfTheWeek['Saterday'] = 6;
    daysOfTheWeek['Sunday'] = 7;
  }

  DateTime meetupDay(int year, int month, String dayofweek, String schedule) {
    //validate paramaters
    String msg = VerifyYearFormat().status(year.toString());
    if (msg != null) throw FormatException(msg);

    if (!dayInstanceString.containsKey(schedule))
      throw ArgumentError(INSTANCEARGUMENTERROR);

    if (!daysOfTheWeek.containsKey(dayofweek)) {
      throw ArgumentError(DAYOFWEEKARGUMENTERROR);
    }

    DateTime findDay = DateTime.utc(year, month);
    return _findNthDay(findDay, daysOfTheWeek[dayofweek], schedule);
  }

  DateTime _findNthDay(
      DateTime startDate, int dayofWeek, String instanceNumber) {
    // finds date of 1st-4th or last weekday
    int instanceOf = dayInstanceString[instanceNumber];
    int counter = 0;
    List<String> days =
        new List(); // store found days a bit easier to work with Formatted Date Strings
    DateTime lookahead;
    DateTime findDay = startDate;
    int month = findDay.month;

    while (counter < instanceOf || month == findDay.month) {
      // search though the month for target days
      if (findDay.weekday == dayofWeek) {
        days.add(new DateFormat('dd/MM/yyyy').format(findDay));
        counter++;
      }

      lookahead = findDay.add(new Duration(days: 1));
      if (lookahead.month != month) // don't loop to next month
        break;
      else
        findDay = findDay.add(new Duration(days: 1));
    }

    if (instanceNumber == 'teenth') {
      // return day-teenths
      final RegExp pattern = new RegExp(
          r'^([1][3-9])'); // from the start of the string match 1 then any number 3-9
      DateTime newDateTimeObj = new DateFormat("dd/MM/yyyy")
          .parse(days[days.indexWhere((days) => days.contains(pattern))]);
      return newDateTimeObj;
    }

    if (instanceOf < days.length) {
      // return 1st though 4th Date
      DateTime newDateTimeObj =
          new DateFormat("dd/MM/yyyy").parse(days[instanceOf - 1]);

      return newDateTimeObj;
    } else {
      // return last date when in a long month
      DateTime newDateTimeObj = new DateFormat("dd/MM/yyyy").parse(days.last);
      return newDateTimeObj;
    }
  }
}
