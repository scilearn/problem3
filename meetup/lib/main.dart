import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:meetup/utilities/verifyyearformat.dart';

import 'meetup/meetupcalc.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Meetup',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Meetup'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of the application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: MeetUpForm(),
    );
  }
}

// A form to collect the statement to Bob
class MeetUpForm extends StatefulWidget {
  @override
  MeetUpFormState createState() {
    return MeetUpFormState();
  }
}

class MeetUpFormState extends State<MeetUpForm> {
  //Global key that  identifies the Form widget and allows validations.
  final _formKey = GlobalKey<FormState>();
  final _yearController = TextEditingController();
  String monthValue = 'January';
  String dayValue = 'Monday';
  String instanceDayValue = '1st';

  DateTime selectedDate = DateTime.now();

  TextEditingController _dateString = new TextEditingController();
  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
      });
  }

  @override
  Widget build(BuildContext context) {
    //A Form widget using the _formKey
    DateTime date;
    return Form(
      key: _formKey,
      child: Center(
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    children: <Widget>[
                      SizedBox(
                        //height: 100,
                        width: 150,
                        child: Column(
                          children: <Widget>[
                            TextFormField(
                                controller: _yearController,
                                decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText: "Which year?",
                                  labelText: 'Year:',
                                ),
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return 'Please enter a 4 digit year';
                                  } else
                                    return VerifyYearFormat().status(value);
                                }),
                            DropdownButton<String>(
                              key: Key('months'),
                              value: monthValue,
                              icon: Icon(Icons.arrow_downward),
                              iconSize: 24,
                              elevation: 16,
                              style: TextStyle(color: Colors.deepPurple),
                              underline: Container(
                                height: 2,
                                color: Colors.deepPurpleAccent,
                              ),
                              onChanged: (String newValue) {
                                setState(() {
                                  monthValue = newValue;
                                });
                              },
                              items: <String>[
                                'January',
                                'Febuary',
                                'March',
                                'April',
                                'May',
                                'June',
                                'July',
                                'Augest',
                                'September',
                                'October',
                                'November',
                                'December'
                              ].map<DropdownMenuItem<String>>((String value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Text(value),
                                );
                              }).toList(),
                            ),
                            DropdownButton<String>(
                              key: Key('days'),
                              value: dayValue,
                              icon: Icon(Icons.arrow_downward),
                              iconSize: 24,
                              elevation: 16,
                              style: TextStyle(color: Colors.deepPurple),
                              underline: Container(
                                height: 2,
                                color: Colors.deepPurpleAccent,
                              ),
                              onChanged: (String newValue) {
                                setState(() {
                                  dayValue = newValue;
                                });
                              },
                              items: <String>[
                                'Monday',
                                'Tuesday',
                                'Wednesday',
                                'Thursday',
                                'Friday',
                                'Saterday',
                                'Sunday'
                              ].map<DropdownMenuItem<String>>((String value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Text(value),
                                );
                              }).toList(),
                            ),
                            DropdownButton<String>(
                              key: Key('instances'),
                              value: instanceDayValue,
                              icon: Icon(Icons.arrow_downward),
                              iconSize: 24,
                              elevation: 16,
                              style: TextStyle(color: Colors.deepPurple),
                              underline: Container(
                                height: 2,
                                color: Colors.deepPurpleAccent,
                              ),
                              onChanged: (String newValue) {
                                setState(() {
                                  instanceDayValue = newValue;
                                });
                              },
                              items: <String>[
                                '1st',
                                '2nd',
                                '3rd',
                                '4th',
                                'last',
                                'teenth'
                              ].map<DropdownMenuItem<String>>((String value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Text(value),
                                );
                              }).toList(),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    children: <Widget>[
                      SizedBox(
                        width: 200,
                        child: TextField(
                          controller: _dateString,
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
            Row(
              children: <Widget>[
                RaisedButton(
                  onPressed: () {
                    if (_formKey.currentState.validate()) {
                      int year = int.parse(_yearController.text);
                      int month = DateFormat("dd/MMMM/yyyy")
                          .parse('01/$monthValue/$year')
                          .month;

                      date = MeetupCalc()
                          .meetupDay(year, month, dayValue, instanceDayValue);
                      _dateString.text =
                          new DateFormat('dd/MM/yyyy').format(date);
                      setState(() {
                        selectedDate = date;
                        _selectDate(context);
                      });

                      // Display Bob's reply in a Snackbar.
                      Scaffold.of(context).showSnackBar(
                          SnackBar(content: Text(date.toString())));
                    }
                  },
                  child: Text('Submit'),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
