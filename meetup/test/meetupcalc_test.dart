import 'package:flutter_test/flutter_test.dart';
import 'package:intl/intl.dart';
import 'package:meetup/meetup/meetupcalc.dart';
import 'package:meetup/utilities/verifyyearformat.dart';

void main() {
  const int YEARNUM = 2020;
  const int MILLENNIUM = 2000;
  const int YEARNEXT = 2021;
  const int FEB = 2;
  const String MON = 'Monday';
  const String WED = 'Wednesday';
  const String FIRSTNUM = '1st'; //"1st", "2nd", "3rd", "4th"
  const String FIRSTTXT = 'first'; // "first", "second", "third", "forth"
  const String THIRDTXT = 'third';
  const int MAR = 3;
  const String SUN = 'Sunday';
  const String TEENTH = 'teenth'; //"last", "teenth"
  const String LAST = 'last';
  const String DAYERROR = 'not a day';
  const String INSTERROR = 'not an instance of day';
  const int FIVENUMYEAR = 20223;

  test('returns DateTime', () {
    expect(
        MeetupCalc().meetupDay(YEARNUM, FEB, MON, FIRSTNUM), isA<DateTime>());
  });

  group('Error Cases', () {
    test('First Error day of Feb', () {
      try {
        MeetupCalc().meetupDay(YEARNUM, FEB, DAYERROR, FIRSTNUM);
      } on ArgumentError catch (e) {
        expect(e.message, MeetupCalc.DAYOFWEEKARGUMENTERROR);
        return;
      }
    });

    test('Error instance of Monday of Feb', () {
      try {
        MeetupCalc().meetupDay(YEARNUM, FEB, MON, INSTERROR);
      } on ArgumentError catch (e) {
        expect(e.message, MeetupCalc.INSTANCEARGUMENTERROR);
        return;
      }
    });

    test('Error YEAR Wednesteenth of Feb', () {
      try {
        MeetupCalc().meetupDay(FIVENUMYEAR, FEB, WED, TEENTH);
      } on FormatException catch (e) {
        expect(e.message, VerifyYearFormat.YEARFORMATEXCEPTION);
        return;
      }
    });
  });

  group('Date Requests', () {
    test('First Monday of Feb', () {
      expect(MeetupCalc().meetupDay(YEARNUM, FEB, MON, FIRSTNUM),
          DateFormat("dd/MM/yyyy").parse('03/$FEB/$YEARNUM'));
      expect(MeetupCalc().meetupDay(YEARNUM, FEB, MON, FIRSTTXT),
          DateFormat("dd/MM/yyyy").parse('03/$FEB/$YEARNUM'));
    });

    test('3rd Sunday of Feb', () {
      expect(MeetupCalc().meetupDay(YEARNUM, FEB, SUN, THIRDTXT),
          DateFormat("dd/MM/yyyy").parse('16/$FEB/$YEARNUM'));
      expect(MeetupCalc().meetupDay(YEARNUM, FEB, SUN, THIRDTXT),
          DateFormat("dd/MM/yyyy").parse('16/$FEB/$YEARNUM'));
    });

    test('Wednesteenth of Feb', () {
      expect(MeetupCalc().meetupDay(YEARNUM, FEB, WED, TEENTH),
          DateFormat("dd/MM/yyyy").parse('19/$FEB/$YEARNUM'));
    });

    //added this test to confirm regex related bug is fixed
    test('Check 9 months of Monteenths for year $MILLENNIUM', () {
      List correctMonTeenths = [
        DateFormat("dd/MM/yyyy").parse('17/01/$MILLENNIUM'),
        DateFormat("dd/MM/yyyy").parse('14/02/$MILLENNIUM'),
        DateFormat("dd/MM/yyyy").parse('13/03/$MILLENNIUM'),
        DateFormat("dd/MM/yyyy").parse('17/04/$MILLENNIUM'),
        DateFormat("dd/MM/yyyy").parse('15/05/$MILLENNIUM'),
        DateFormat("dd/MM/yyyy").parse('19/06/$MILLENNIUM'),
        DateFormat("dd/MM/yyyy").parse('17/07/$MILLENNIUM'),
        DateFormat("dd/MM/yyyy").parse('14/08/$MILLENNIUM'),
        DateFormat("dd/MM/yyyy").parse('18/09/$MILLENNIUM')
      ];
      for (int i = 1; i < 8; i++) {
        expect(MeetupCalc().meetupDay(MILLENNIUM, i, MON, TEENTH),
            correctMonTeenths[i - 1]);
      }
    });

    test('Last Wensday of March Next Year', () {
      expect(MeetupCalc().meetupDay(YEARNEXT, MAR, WED, LAST),
          DateFormat("dd/MM/yyyy").parse('31/$MAR/$YEARNEXT'));
    });
  });
}
